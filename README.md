# Atividade Prática



## Questão 1

O Git é uma ferramenta de controle de versão de código fonte, repositório. Ele permite que todas as mudanças no código fonte de um software sejam armazenadas e rastreáveis. Podendo ficar remotamente ou localmente na máquina.

## Questão 2
Stating área é uma ramificação criada a partir do ramo Master de um código fonte. Onde será realizado todos os testes existentes, testes regressivos, nas modificações realizadas no código. E só depois de ser aprovado nos testes é que o software irá receber sua nova versão(mesclada) no branch Master. 
## Questão 3
Working directory são todos arquivos trabalhados no momento.
## Questão 4
Commit é um instantâneo das modificações adicionadas na staging area, ficam no repositório local.
## Questão 5
Branch é uma ramificação do código. O padrão é o master.
## Questão 6
Head no Git aponta para algum commit ou algum branch.
## Questão 7
Merge é a ação de juntar os commits de dois branches.
## Questão 8
4 estados de um arquivo no git: 
*untracked : arquivos que não estavam no ultimo commit;
*unmodified: arquivos não modificados no ultimo commit;
*modified: arquivos modificados no ultimo commit;
*staged: arquivos preparados para comitar.
## Questão 9
git init é um comando necessário para iniciar um diretório local na máquina. Ele também cria um subdiretório com todos os metadados necessários.
## Questão 10
git add é um comando que adiciona um arquivo na staging area.
## Questão 11
git status é um comando utilizado para verificar se o arquivo README.md foi adicionado a staging area.
## Questão 12
git commit prepara os arquivos alterados para serem versionados. Somente os arquivos que sofreram commit terão suas versões controladas pelo git.
## Questão 13
git log é um comando utilizado para mostrar os commits realizados no nosso repositório(histórico).
## Questão 14
git checkout –b é um comando utilizado para criar novas ramificações(branches).
## Questão 15
git reset é um comando utilizado para desfazer um commit. Existem 3 opções:
*soft: move o HEAD para o commit indicado e mantém a staging area e o working directory inalterados.
*mixed: move o HEAD para o commit indicado, altera a staging area e mantém o working directory inalterado.
*hard: todas as alterações realizadas após o commit em que retornamos serão perdidas.
## Questão 16
git revert é um comando que cria um novo commit com as alterações do commit que foi indicado.
## Questão 17
git clone é um comando utilizado para baixar um repositório de um projeto no gitlab para a nossa máquina.
## Questão 18
git push é um comando para enviar arquivos para o repositório remoto.
## Questão 19
git pull é o comando utilizado para baixar o repositório remoto e fazer a atualização junto ao repositório local.
## Questão 20
Para ignorar o versionamento de códigos no Git devemos cria um arquivo do tipo .gitignore.
## Questão 21
As branches são ramificações criadas no para alocar os projetos criados ou modificados. Para, depois, receberem um commit e assim, serem controlados pelo Git. São elas:
*master: é o branch padrão;
*main: é o branch principal;
*develop: é um ramo para modificações de correção ou evolução de funcionalidades;
*staging: local temporário onde as alterações aguardam para receberem o próximo commit, e onde são realizado os testes para aprovação das modificações dos códigos. 
